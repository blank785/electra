# Databricks notebook source
from model.tokenization import WordpieceTokenizer

# COMMAND ----------

from transformers import BertTokenizer

# Initialize the BERT tokenizer
tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

# Example text
text = "SCB TechX, an SCBX company specializing in digital technology, has introduced PointX, the latest platform development. It is a new world that lets reward point collectors experience unlimited reward point accumulation and redemption. With a concept of all-in-one reward point wallet platform, customers can use reward points like cash for purchases at any shops displaying the PointX logo, get discount coupons for shopping for flash deals and special priced items via an in-app X Store, transfer and share reward points, and enjoy using reward points at bonus rates every day. The PointX application is starting pilot services for Siam Commercial Bank (SCB) credit cardholders with reward point programs before rolling out to SCB’s other customer segments and other business partners in the future."


# Tokenize the text using WordPiece
tokens = tokenizer.tokenize(text)
print(tokens)


# COMMAND ----------

# MAGIC %md
# MAGIC ## Fine tune

# COMMAND ----------

from collections import Counter
from transformers import BertTokenizer

# Load the existing tokenizer with the pre-defined vocab file
vocab_file_path = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/vocab.txt"
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

# Function to tokenize the domain-specific documents and update the vocabulary
def update_vocabulary_from_documents(documents_folder):
    # Initialize a counter to count the frequency of tokens
    token_counter = Counter()

    # Tokenize the domain-specific documents and update the token_counter
    for file_name in os.listdir(documents_folder):
        with open(os.path.join(documents_folder, file_name), "r", encoding="utf-8") as file:
            document = file.read()
            print(document)
            tokens = tokenizer.tokenize(document)
            token_counter.update(tokens)

    # Add new tokens and their frequencies to the tokenizer's vocabulary
    for token, count in token_counter.items():
        print(token, count)
        if token not in tokenizer.vocab:
            tokenizer.add_tokens([token])
            print("new token", token)

    # Save the updated vocabulary to a new file
    new_vocab_file_path = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/fine_tuned_vocab.txt"
    tokenizer.save_vocabulary(new_vocab_file_path)

    return new_vocab_file_path

# Example usage
if __name__ == "__main__":
    import os

    # Provide the path to your raw data folder
    raw_data_folder = "/dbfs/FileStore/tds/peerasak/eletra-resource/for_retrain/raw"

    # Update the vocabulary using domain-specific documents
    new_vocab_file_path = update_vocabulary_from_documents(raw_data_folder)

    print("Updated vocabulary file saved at:", new_vocab_file_path)


# COMMAND ----------

with open("/dbfs/FileStore/tds/peerasak/eletra-resource/data/fine_tuned_vocab.txt") as new_tk:
    ls_tk = new_tk.readlines()
    for t in ls_tk:
        if t.startswith('scb'):
            print(t)

# COMMAND ----------

new_vocab_file_path

# COMMAND ----------

new_tokenizer = BertTokenizer.from_pretrained(new_vocab_file_path)
new_tokens = new_tokenizer.tokenize(text)

# COMMAND ----------

print(new_tokens)

# COMMAND ----------

len(tokens), len(new_tokens)

# COMMAND ----------

tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

# Example text
text = "SCB TechX, an SCBX company specializing in digital technology, has introduced PointX, the latest platform development. It is a new world that lets reward point collectors experience unlimited reward point accumulation and redemption. With a concept of all-in-one reward point wallet platform, customers can use reward points like cash for purchases at any shops displaying the PointX logo, get discount coupons for shopping for flash deals and special priced items via an in-app X Store, transfer and share reward points, and enjoy using reward points at bonus rates every day. The PointX application is starting pilot services for Siam Commercial Bank (SCB) credit cardholders with reward point programs before rolling out to SCB’s other customer segments and other business partners in the future. SCB TechX expects some 300,000 downloads of the PointX application by the end of 2022. SCB TechX Chief Executive Officer Mr. Trirat Suwanprateeb said, SCB TechX is developing new capabilities in advanced digital technology, platforms, and innovation to add value and develop new business models for business partners and corporations as well as SCBX companies. PointX is our latest platform development addressing a major pain point among reward point collectors, that is, their reward points are scattered across different platforms. With a development concept of all-in-one reward point wallet platform, the PointX application offers a new world of reward point accumulation and redemption that unlocks customers from the way they used to deal with reward points. In the first phase, we have joined hands with SCB to offer pilot services to SCB credit cardholders with reward point programs as the first group to try this new experience, before rolling out to other SCB customer segments. We also welcome business partners interested in the platform and who see the benefits of adding business value and developing an ecosystem to promote and drive a new world of using reward points like cash. We expect to see some 300,000 customers downloading the PointX application by the end of this year. Speaking about the new application, SCB Senior Executive Vice President and Chief Wealth Banking Officer Mr. Yunyong Thaicharoen noted that, SCB is continuously offering new financial experiences in response to our customers’ changing lifestyles in a new world context. According to a joint study of SCB cardholders conducted by SCB and SCB TechX, most SCB cardholders holding reward points are in the SCB Wealth segment and enjoy higher purchasing power. More specifically, there are two main groups. The first group contains SCB cardholders having between 5,000 – 50,000 reward points. They usually redeem reward points for shopping at shopping malls, obtaining discounts, and for wealth investment. The second group is composed of those having between 50,000 – 500,000 reward points. They redeem reward points at a higher rate than other groups, mostly for shopping at shopping malls and travel deals. These two groups account for some 60% of SCB credit cardholders in reward point programs. We do hope that this partnership will unlock SCB Wealth credit cardholders’ ability including SCB PRIVATE BANKING, SCB FIRST and SCB PRIME to experience unlimited reward point accumulation and redemption and enjoy even more daily spending."

text_2 = "The overwhelming amount of biomedical scientific texts calls for the development of effective language models able to tackle a wide range of biomedical natural language processing (NLP) tasks. The most recent dominant approaches are domain-specific models, initialized with general-domain textual data and then trained on a variety of scientific corpora. However, it has been observed that for specialized domains in which large corpora exist, training a model from scratch with just in-domain knowledge may yield better results. Moreover, the increasing focus on the compute costs for pre-training recently led to the design of more efficient architectures, such as ELECTRA. In thispaper, we propose a pre-trained domain-specific language model, called ELECTRAMed, suited for the biomedical field. The novel approach inherits the learning framework of the general-domain ELECTRA architecture, as well as its computational advantages. Experiments performed on benchmark datasets for several biomedical NLP tasks support the usefulness of ELECTRAMed, which sets the novel state-of-the-art result on the BC5CDR corpus for named entity recognition, and provides the best outcome in 2 over the 5 runs of the 7th BioASQ-factoid Challange for the question answering task."
# Tokenize the text using WordPiece
tokens = tokenizer.tokenize(text_2)

# COMMAND ----------

len(tokens)

# COMMAND ----------

new_tokenizer = BertTokenizer.from_pretrained(new_vocab_file_path)
new_t = new_tokenizer.tokenize(text_2)

# COMMAND ----------

len(new_t)

# COMMAND ----------

new_t

# COMMAND ----------

from collections import Counter

class WordPieceTokenizer:
    def __init__(self, vocab, unk_token='[UNK]', max_word_length=100):
        self.vocab = vocab
        self.unk_token = unk_token
        self.max_word_length = max_word_length

    def tokenize(self, text):
        tokens = []
        start = 0

        while start < len(text):
            end = len(text)
            cur_substr = None

            while start < end:
                substr = text[start:end]
                if start > 0:
                    substr = "##" + substr  # Add "##" prefix to subword pieces
                if substr in self.vocab or len(substr) == 3:  # Check if in vocab or if it's a single character
                    cur_substr = substr
                    break
                end -= 1

            if cur_substr is None:  # If no subword found, use the unknown token
                cur_substr = self.unk_token

            tokens.append(cur_substr)
            start = end

        return tokens


def build_vocabulary(documents, max_vocab_size, min_subword_freq):
    word_counter = Counter()
    for doc in documents:
        # Preprocess your documents if needed (e.g., remove special characters, lowercase)
        words = doc.lower().split()  # Tokenize words (you can use a better tokenizer if available)
        word_counter.update(words)

    # Sort the words by frequency in descending order
    sorted_words = sorted(word_counter.items(), key=lambda x: x[1], reverse=True)

    # Select the most frequent subwords up to the max_vocab_size and above the min_subword_freq threshold
    vocab = {}
    subword_count = 0
    for word, count in sorted_words:
        if subword_count >= max_vocab_size:
            break
        if count >= min_subword_freq:
            vocab[word] = count
            subword_count += 1

    return vocab


# Example usage:
documents = [
    "SCB TechX, an SCBX company specializing in digital technology, has introduced PointX, the latest platform development. It is a new world that lets reward point collectors experience unlimited reward point accumulation and redemption. With a concept of all-in-one reward point wallet platform, customers can use reward points like cash for purchases at any shops displaying the PointX logo, get discount coupons for shopping for flash deals and special priced items via an in-app X Store, transfer and share reward points, and enjoy using reward points at bonus rates every day. The PointX application is starting pilot services for Siam Commercial Bank (SCB) credit cardholders with reward point programs before rolling out to SCB’s other customer segments and other business partners in the future. SCB TechX expects some 300,000 downloads of the PointX application by the end of 2022. SCB TechX Chief Executive Officer Mr. Trirat Suwanprateeb said, SCB TechX is developing new capabilities in advanced digital technology, platforms, and innovation to add value and develop new business models for business partners and corporations as well as SCBX companies. PointX is our latest platform development addressing a major pain point among reward point collectors, that is, their reward points are scattered across different platforms. With a development concept of all-in-one reward point wallet platform, the PointX application offers a new world of reward point accumulation and redemption that unlocks customers from the way they used to deal with reward points. In the first phase, we have joined hands with SCB to offer pilot services to SCB credit cardholders with reward point programs as the first group to try this new experience, before rolling out to other SCB customer segments. We also welcome business partners interested in the platform and who see the benefits of adding business value and developing an ecosystem to promote and drive a new world of using reward points like cash. We expect to see some 300,000 customers downloading the PointX application by the end of this year. Speaking about the new application, SCB Senior Executive Vice President and Chief Wealth Banking Officer Mr. Yunyong Thaicharoen noted that, SCB is continuously offering new financial experiences in response to our customers’ changing lifestyles in a new world context. According to a joint study of SCB cardholders conducted by SCB and SCB TechX, most SCB cardholders holding reward points are in the SCB Wealth segment and enjoy higher purchasing power. More specifically, there are two main groups. The first group contains SCB cardholders having between 5,000 – 50,000 reward points. They usually redeem reward points for shopping at shopping malls, obtaining discounts, and for wealth investment. The second group is composed of those having between 50,000 – 500,000 reward points. They redeem reward points at a higher rate than other groups, mostly for shopping at shopping malls and travel deals. These two groups account for some 60% of SCB credit cardholders in reward point programs. We do hope that this partnership will unlock SCB Wealth credit cardholders’ ability including SCB PRIVATE BANKING, SCB FIRST and SCB PRIME to experience unlimited reward point accumulation and redemption and enjoy even more daily spending."
]


max_vocab_size = 1000
min_subword_freq = 2
new_vocab = build_vocabulary(documents, max_vocab_size, min_subword_freq)
print(new_vocab)


# COMMAND ----------

from collections import Counter
from transformers import BertTokenizer

# Load the existing tokenizer with the pre-defined vocab file
vocab_file_path = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/vocab.txt"
tokenizer = BertTokenizer.from_pretrained(vocab_file_path)

# Function to tokenize the domain-specific documents and update the vocabulary
def update_vocabulary_from_documents(new_vocab):
    # Add new tokens and their frequencies to the tokenizer's vocabulary
    for token, count in new_vocab.items():
        # print(token, count)
        if token not in tokenizer.vocab:
            tokenizer.add_tokens([token])
            # print("new token", token)
            #print(token in tokenizer.vocab)

    # Save the updated vocabulary to a new file
    new_vocab_file_path = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/fine_tuned_vocab.txt"
    tokenizer.save_vocabulary(new_vocab_file_path)
    temp_text ="SCB TechX, an SCBX company specializing in digital technology, has introduced PointX, the latest platform development. It is a new world that lets reward point collectors experience unlimited reward point accumulation and redemption. With a concept of all-in-one reward point wallet platform, customers can use reward points like cash for purchases at any shops displaying the PointX logo, get discount coupons for shopping for flash deals and special priced items via an in-app X Store, transfer and share reward points, and enjoy using reward points at bonus rates every day. The PointX application is starting pilot services for Siam Commercial Bank (SCB) credit cardholders with reward point programs before rolling out to SCB’s other customer segments and other business partners in the future."

    print(tokenizer.tokenize(temp_text))
    return new_vocab_file_path
# Example usage
if __name__ == "__main__":
    import os

    # Provide the path to your raw data folder
    raw_data_folder = "/dbfs/FileStore/tds/peerasak/eletra-resource/for_retrain/raw"

    # Update the vocabulary using domain-specific documents
    new_vocab_file_path = update_vocabulary_from_documents(new_vocab)

    print("Updated vocabulary file saved at:", new_vocab_file_path)


# COMMAND ----------

new_tokenizer = BertTokenizer.from_pretrained(new_vocab_file_path)
new_t = new_tokenizer.tokenize(text)

# COMMAND ----------

new_t

# COMMAND ----------

for t in new_tokenizer.vocab:
    if t[0] == 'scb':
        print(t[0], i)

# COMMAND ----------

'sc' in new_tokenizer.vocab

# COMMAND ----------

tokenizer = BertTokenizer.from_pretrained(new_vocab_file_path)

# Print the vocabulary to check if the new token is present
print(tokenizer.get_vocab())

# COMMAND ----------

'scbx' in tokenizer.vocab

# COMMAND ----------


