# Databricks notebook source
from collections import Counter
from transformers import BertTokenizer
import os

# COMMAND ----------

# MAGIC %md
# MAGIC ### Config params

# COMMAND ----------

domain_corpus_dir = "/dbfs/FileStore/tds/peerasak/eletra-resource/for_retrain/raw"
max_vocab_size = 1000
min_subword_freq = 2
vocab_file_path = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/vocab.txt"
new_vocab_file_path = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/vocab.txt"
tokenizer = BertTokenizer.from_pretrained(vocab_file_path)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Main modules

# COMMAND ----------

class WordPieceTokenizer:
    def __init__(self, vocab, unk_token='[UNK]', max_word_length=100):
        self.vocab = vocab
        self.unk_token = unk_token
        self.max_word_length = max_word_length

    def tokenize(self, text):
        tokens = []
        start = 0

        while start < len(text):
            end = len(text)
            cur_substr = None

            while start < end:
                substr = text[start:end]
                if start > 0:
                    substr = "##" + substr  # Add "##" prefix to subword pieces
                if substr in self.vocab or len(substr) == 3:  # Check if in vocab or if it's a single character
                    cur_substr = substr
                    break
                end -= 1

            if cur_substr is None:  # If no subword found, use the unknown token
                cur_substr = self.unk_token

            tokens.append(cur_substr)
            start = end

        return tokens


def build_vocabulary(tokenizer, documents, max_vocab_size, min_subword_freq):
    token_counter = Counter()
    for doc in documents:
        tokens = doc.lower().split() 
        token_counter.update(tokens)
        
        # Tokenize words (you can use a better tokenizer if available)
        # doc = doc.lower()
        # tokens = tokenizer.tokenize(doc)
        # token_counter.update(tokens)
        


    # Sort the words by frequency in descending order
    sorted_tokens = sorted(token_counter.items(), key=lambda x: x[1], reverse=True)

    # Select the most frequent subwords up to the max_vocab_size and above the min_subword_freq threshold
    vocab = {}
    subword_count = 0
    for tk, count in sorted_tokens:
        if subword_count >= max_vocab_size:
            break
        if count >= min_subword_freq:
            vocab[tk] = count
            subword_count += 1

    return vocab

def update_vocabulary_from_documents(tokenizer, new_vocab, new_vocab_file):
    # Add new tokens and their frequencies to the tokenizer's vocabulary
    print("Start update vocab")
    print("No. of vocabs before adding: ", len(tokenizer.vocab))
    for token, count in new_vocab.items():
        # print(token, count)
        if token not in tokenizer.vocab:
            tokenizer.add_tokens([token])
            # print("new token", token)
            #print(token in tokenizer.vocab)
    ls_tk = list(tokenizer.get_vocab().keys())
    print("No. of vocabs after adding: ", len(ls_tk))
    with open(new_vocab_file, 'w') as f:
        f.writelines('\n'.join(ls_tk))
    f.close()
    return True

# COMMAND ----------

# MAGIC %md
# MAGIC ## Start

# COMMAND ----------

documents = []
for file_name in os.listdir(domain_corpus_dir):
        file_path = os.path.join(domain_corpus_dir, file_name)
        print("read file: ", file_path)
        with open(file_path, "r", encoding="utf-8") as file:

            document = file.read()
            documents.append(document)
            
## build vocab
new_vocab = build_vocabulary(tokenizer, documents, max_vocab_size, min_subword_freq)

## save new vocab
update_vocabulary_from_documents(tokenizer, new_vocab,new_vocab_file_path)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC ## NOT USED PART
# MAGIC - For test results

# COMMAND ----------

update_vocabulary_from_documents(tokenizer, new_vocab,new_vocab_file_path)


# COMMAND ----------

#compare file
with open(vocab_file_path) as old_f:
    old_lines = old_f.read().splitlines()

with open(new_vocab_file_path) as new_f:
    new_lines = new_f.read().splitlines()

# COMMAND ----------

len(old_lines), len(new_lines)

# COMMAND ----------

'scb' in list(tokenizer.get_vocab().keys())

# COMMAND ----------

tokenizer_with_old = BertTokenizer.from_pretrained(vocab_file_path)
tokenizer_with_new = BertTokenizer.from_pretrained(new_vocab_file_path)

# COMMAND ----------

test_text = 'SCB TechX, an SCBX company specializing in digital technology, has introduced PointX, the latest platform development. It is a new world that lets reward point collectors experience unlimited reward point accumulation and redemption. With a concept of all-in-one reward point wallet platform, customers can use reward points like cash for purchases at any shops displaying the PointX logo, get discount coupons for shopping for flash deals and special priced items via an in-app X Store, transfer and share reward points, and enjoy using reward points at bonus rates every day. The PointX application is starting pilot services for Siam Commercial Bank (SCB) credit cardholders with reward point programs before rolling out to SCB’s other customer segments and other business partners in the future.'

# COMMAND ----------

tk_old = tokenizer_with_old.tokenize(test_text)

# COMMAND ----------

tk_new = tokenizer_with_new.tokenize(test_text)

# COMMAND ----------

len(tk_old), len(tk_new)

# COMMAND ----------

tk_old

# COMMAND ----------

tk_new

# COMMAND ----------


