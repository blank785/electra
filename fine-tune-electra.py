# Databricks notebook source
import pandas as pd
import os

# COMMAND ----------

# MAGIC %md
# MAGIC ## Create dataset from raw data
# MAGIC - Use "build_pretraining_dataset.py" to create a pre-training dataset from a dump of raw text.
# MAGIC It has the following arguments:
# MAGIC
# MAGIC - --corpus-dir: A directory containing raw text files to turn into ELECTRA examples. A text file can contain multiple documents with empty lines separating them.
# MAGIC - --vocab-file: File defining the wordpiece vocabulary.
# MAGIC - --output-dir: Where to write out ELECTRA examples.
# MAGIC - --max-seq-length: The number of tokens per example (128 by default).
# MAGIC - --num-processes: If >1 parallelize across multiple processes (1 by default).
# MAGIC - --blanks-separate-docs: Whether blank lines indicate document boundaries (True by default).
# MAGIC - --do-lower-case/--no-lower-case: Whether to lower case the input text (True by default).

# COMMAND ----------

os.environ['data_dir'] = "/dbfs/FileStore/tds/peerasak/eletra-resource/data"
os.environ['corpus_dir'] = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/raw_data"
os.environ['vocab_file'] = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/vocab.txt"
os.environ['output_dir'] = "/dbfs/FileStore/tds/peerasak/eletra-resource/data/pretrain_tfrecords"
os.environ['model_name'] = 'electra_large'

# COMMAND ----------

# dbutils.fs.rm('dbfs:/FileStore/tds/peerasak/eletra-resource/data/pretrain_tfrecords',True)

# COMMAND ----------

# dbutils.fs.cp("dbfs:/FileStore/tds/peerasak/eletra-resource/electra_large", "dbfs:/FileStore/tds/peerasak/eletra-resource/data/eletra_large", True)

# COMMAND ----------

# MAGIC %sh
# MAGIC python build_pretraining_dataset.py --corpus-dir $corpus_dir --vocab-file $vocab_file --output-dir $output_dir

# COMMAND ----------

# MAGIC %md
# MAGIC ## Pretrain
# MAGIC
# MAGIC Use "run_pretraining.py" to pre-train an ELECTRA model. It has the following arguments:
# MAGIC
# MAGIC - --data-dir: a directory where pre-training data, model weights, etc. are stored. By default, the training loads examples from <data-dir>/pretrain_tfrecords and a vocabulary from <data-dir>/vocab.txt.
# MAGIC - --model-name: a name for the model being trained. Model weights will be saved in <data-dir>/models/<model-name> by default.
# MAGIC - --hparams (optional): a JSON dict or path to a JSON file containing model hyperparameters, data paths, etc. See configure_pretraining.py for the supported hyperparameters.

# COMMAND ----------

import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

# COMMAND ----------



# COMMAND ----------

# dbutils.fs.rm("dbfs:/FileStore/tds/peerasak/eletra-resource/data/models/base_model",True)

# COMMAND ----------

# dbutils.fs.cp("dbfs:/FileStore/tds/peerasak/eletra-resource/base_model", "dbfs:/FileStore/tds/peerasak/eletra-resource/data/"+os.getenv('model_name'), True)

# COMMAND ----------

# MAGIC %sh
# MAGIC python run_pretraining.py --data-dir $data_dir --model-name $model_name

# COMMAND ----------

# MAGIC %md
# MAGIC ## Evaluation
# MAGIC - To evaluate the model on a downstream task, see the below finetuning instructions.
# MAGIC - To evaluate the generator/discriminator on the openwebtext data run:
# MAGIC   -  python run_pretraining.py --data-dir $DATA_DIR --model-name electra_small_owt --hparams '{"do_train": false, "do_eval": true}'.
# MAGIC
# MAGIC - This will print out eval metrics such as the accuracy of the generator and discriminator, and also writing the metrics out to data-dir/model-name/results.

# COMMAND ----------

# MAGIC %sh
# MAGIC python run_pretraining.py --data-dir $data_dir --model-name "electra_large" --hparams '{"do_train": false, "do_eval": true}'

# COMMAND ----------


